<?php

namespace AppBundle\EventListener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use AppBundle\Entity\Interfaces\Timestampable;

/**
 * TimestampListener tracks createdAt and updatedAt timestamps for all
 * AppBundle\Entity\Interfaces\Timestampable implementations
 *
 * @author andrew
 */
class TimestampListener
{

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if ($entity instanceof Timestampable) {
            $entity->setCreatedAt(new \DateTime('now'));
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getEntity();
        if (!$entity instanceof Timestampable) {
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }

}
