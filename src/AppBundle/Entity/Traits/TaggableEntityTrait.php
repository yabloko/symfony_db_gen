<?php

namespace AppBundle\Entity\Traits;

/**
 * TaggableEntityTrait provides reusable code for 
 * Interfaces\TaggablEntity implementation,
 * 
 * @author andrew
 */
trait TaggableEntityTrait
{

    /**
     * add tags
     *
     * @param \IteratorAggregate $tags
     *
     * @return ApplicationUser
     */
    public function addTags($tags)
    {
        foreach ($tags as $tag) {
            $this->tags->add($tag);
        }
        return $this;
    }

    public function removeTag($tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return ArrayCollection
     */
    public function getTags()
    {
        return $this->tags;
    }

}
