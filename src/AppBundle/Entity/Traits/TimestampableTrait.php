<?php

namespace AppBundle\Entity\Traits;

/**
 * TimestampableTrait provides reusable methods for
 * AppBundle\Entitiy\Interfaces\Timestampable implementations
 * and requires implementation to have following attributes with proper annotations
 * to store them in DB:
 * protected $createdAt
 * protected $updatedAt
 *
 * @author andrew
 */
trait TimestampableTrait
{

    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;
        return $this;
    }

    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;
        return $this;
    }

    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

}
