<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Post
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Post implements Interfaces\TaggableEntity, Interfaces\Timestampable
{

    use Traits\TaggableEntityTrait,
        Traits\TimestampableTrait;

    /**
     * @var integer
     * @Serializer\Groups({"read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     * @Serializer\Groups({"read","create"})
     * @ORM\Column(name="application_user_id", type="integer")
     */
    private $userId;

    /**
     * @var string
     * @Assert\Regex(
     *      pattern="/^[\w\s]+$/",
     *      message="post can contain only alphanumeric characters or space"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 1023,
     *      minMessage = "post must be at least {{ limit }} characters long",
     *      maxMessage = "post cannot be longer than {{ limit }} characters"
     * )
     * @Serializer\Groups({"read","create","update"})
     * @ORM\Column(name="body", type="string", length=1023)
     */
    private $body;

    /**
     * @var \DateTime
     * @Serializer\Groups({"read"})
     * @ORM\Column(name="createdAt", type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     * @Serializer\Groups({"read"})
     * @ORM\Column(name="updatedAt", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @ORM\ManyToOne(targetEntity="ApplicationUser", inversedBy="posts")
     * @ORM\JoinColumn(name="application_user_id", referencedColumnName="id", onDelete="cascade")
     * @var ApplicationUser
     */
    private $user;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="posts")
     * @ORM\JoinTable(name="post_tags")
     */
    private $tags;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set body
     *
     * @param string $body
     *
     * @return Post
     */
    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    /**
     * Get body
     *
     * @return string
     */
    public function getBody()
    {
        return $this->body;
    }

    public function getUserId()
    {
        return $this->userId;
    }

    public function setUserId($userId)
    {
        $this->userId = $userId;
        return $this;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser(ApplicationUser $user)
    {
        $this->user = $user;
        return $this;
    }

}
