<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Tag
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class Tag
{

    /**
     * @var integer
     * @Serializer\Groups({"read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Regex(
     *      pattern="/^#[a-z\d]+$/",
     *      message="tag should start with # and contain only lowercase letters ot numbers"
     * )
     * @Assert\Length(
     *      min = 4,
     *      max = 31,
     *      minMessage = "tag must be at least {{ limit }} characters long",
     *      maxMessage = "tag cannot be longer than {{ limit }} characters"
     * )
     * @Serializer\Groups({"read","create","update"})
     * @ORM\Column(name="name", type="string", length=31)
     */
    private $name;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="ApplicationUser", mappedBy="tags")
     * @var \Doctrine\Common\Collections\ArrayCollection 
     */
    private $users;

    /**
     * 
     * @ORM\ManyToMany(targetEntity="Post", mappedBy="tags")
     * @var \Doctrine\Common\Collections\ArrayCollection 
     */
    private $posts;

    public function __construct()
    {
        $this->users = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Tag
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function getUsers()
    {
        return $this->users;
    }

    public function getPosts()
    {
        return $this->posts;
    }

}
