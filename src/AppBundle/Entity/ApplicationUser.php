<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ApplicationUser
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ApplicationUser implements Interfaces\TaggableEntity
{

    use Traits\TaggableEntityTrait;

    /**
     * @var integer
     * @Serializer\Groups({"read"})
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     * @Assert\Regex(
     *      pattern="/^[a-zA-Z\s\.]+$/",
     *      message="name can contain only letters spaces and dot"
     * )
     * @Assert\Length(
     *      min = 2,
     *      max = 255,
     *      minMessage = "name must be at least {{ limit }} characters long",
     *      maxMessage = "name cannot be longer than {{ limit }} characters"
     * )
     * @Serializer\Groups({"read","create","update"})
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     * @Assert\Email(
     *     message = "login should be a valid email"
     * )
     * @Serializer\Groups({"read","create","update"})
     * @ORM\Column(name="login", type="string", length=255)
     */
    private $login;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="Tag", inversedBy="users")
     * @ORM\JoinTable(name="user_tags")
     */
    private $tags;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\OneToMany(targetEntity="Post", mappedBy="user", cascade={"persist"})
     * @ORM\JoinColumn(name="id", referencedColumnName="application_user_id")
     */
    private $posts;

    public function __construct()
    {
        $this->tags = new ArrayCollection();
        $this->posts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    /**
     * silently trim input name
     * @param string $name
     * @return \AppBundle\Entity\ApplicationUser
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    public function getLogin()
    {
        return $this->login;
    }

    public function setLogin($login)
    {
        $this->login = $login;
        return $this;
    }

    /**
     * Get posts
     *
     * @return ArrayCollection
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * add post
     *
     * @param \AppBundle\Entity\Post $post
     *
     * @return ApplicationUser
     */
    public function addPost($post)
    {
        $post->setUser($this);
        $this->posts->add($post);
        return $this;
    }

}
