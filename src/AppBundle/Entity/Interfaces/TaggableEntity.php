<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Entity\Interfaces;

/**
 *
 * @author andrew
 */
interface TaggableEntity
{

    /**
     * add tags
     *
     * @param \IteratorAggregate $tags
     *
     * @return ApplicationUser
     */
    public function addTags($tags);

    /**
     * remove tag from ArrayCollection<Tag>
     * @param type $tag
     */
    public function removeTag($tag);

    /**
     * Get tags
     *
     * @return ArrayCollection
     */
    public function getTags();
}
