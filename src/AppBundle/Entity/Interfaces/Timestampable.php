<?php

namespace AppBundle\Entity\Interfaces;

/**
 * Timestampable entity allows to set a createdAt and updatedAt \DateTime 
 * 
 * @author andrew
 */
interface Timestampable
{
    public function setCreatedAt(\DateTime $createdAt);
    
    public function setUpdatedAt(\DateTime $updatedAt);
    
    public function getCreatedAt();
    
    public function getUpdatedAt();
}
