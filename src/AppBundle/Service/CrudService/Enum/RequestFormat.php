<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Service\CrudService\Enum;

use MyCLabs\Enum\Enum;

/**
 * Description of RequestFormat
 *
 * @author andrew
 */
class RequestFormat extends Enum
{
    const JSON = 'json';
    const XML = 'xml';
}
