<?php

namespace AppBundle\Service\CrudService\Http;

use Symfony\Component\HttpFoundation\Request;

/**
 * CrudHttpRequest is an adapter to http request object type for CrudService
 *
 * @author andrew
 */
class CrudHttpRequest
{

    /**
     *
     * @var Symfony\Component\HttpFoundation\Request
     */
    private $request;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    public function getContent()
    {
        $this->request->getContent();
    }

}
