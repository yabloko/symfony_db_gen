<?php

namespace AppBundle\Service\CrudService\Http;

use Symfony\Component\HttpFoundation\Response;

/**
 * CrudHttpResponse is an adapter, a service specific representation 
 * of http response object, it contains methods to convert 
 * self to other http response types
 * currently it supports 200, 201, 202 and 404 statuses
 *
 * @author andrew
 */
class CrudHttpResponse
{

    const HTTP_OK = 200;
    const HTTP_CREATED = 201;
    const HTTP_ACCEPTED = 202;
    const HTTP_NOT_FOUND = 404;

    private $body;
    private $status;

    public function __construct($body, $status = self::HTTP_OK)
    {
        $this->body = $body;
        $this->status = $status;
    }

    public function toResponse()
    {
        return new Response($this->body, $this->status);
    }

    public function getBody()
    {
        return $this->body;
    }

    public function getStatus()
    {
        return $this->status;
    }

}
