<?php

namespace AppBundle\Service\CrudService;

use AppBundle\Service\CrudService\Http\CrudHttpRequest;
use AppBundle\Service\CrudService\Http\CrudHttpResponse;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * CrudService
 *
 * @author andrew
 */
class CrudService implements Interfaces\RestCrudServiceInterface
{

    protected $serializer;
    protected $repository;
    protected $entityManager;
    protected $annotationParser;
    protected $validator;
    protected $format;

    public function __construct(SerializerInterface $serializer, EntityManagerInterface $manager, Interfaces\AnnotationParserInterface $annotationParser, ValidatorInterface $validator = null)
    {
        $this->format = Enum\RequestFormat::JSON;
        $this->serializer = $serializer;
        $this->entityManager = $manager;
        $this->annotationParser = $annotationParser;
        $this->validator = $validator;
    }

    public function create(CrudHttpRequest $request)
    {
        return $this->upsertRequest($request->getContent(), $this->repository);
    }

    public function delete($entityId)
    {
        $entity = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new CrudHttpResponse('', 404);
        }
        $this->entityManager->remove($entity);
        $this->entityManager->flush();
        return new CrudHttpResponse('', 200);
    }

    public function listAll()
    {
        $entities = $this->entityManager
                ->getRepository($this->repository)
                ->findAll();
        $response = $this->serialize($entities);
        return new CrudHttpResponse($response);
    }

    public function read($entityId)
    {
        $entity = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new CrudHttpResponse('', 404);
        }
        return new CrudHttpResponse($this->serialize($entity));
    }

    public function update($entityId, CrudHttpRequest $request)
    {
        $entity = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new CrudHttpResponse('', 404);
        }
        return $this->upsertRequest($request->getContent(), $this->repository);
    }

    public function addManyToMany($entityId, $relatedRepository, CrudHttpRequest $request)
    {
        $addMethodName = $this->annotationParser->getAddMethod($this->repository, $relatedRepository);
        $deserializerRepository = 'ArrayCollection<' . $relatedRepository . '>';
        $requestedEntities = $this->deserialize($request->getContent(), $deserializerRepository);
        $ids = array_map(function($related) {
            return $related->getId();
        }, $requestedEntities);
        $entity = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        $relatedEntities = $this->entityManager
                ->getRepository($relatedRepository)
                ->findBy(array('id' => $ids));
        if (!$entity || count($relatedEntities) < 1) {
            return new CrudHttpResponse('', 404);
        }
        $entity->$addMethodName($relatedEntities);
        $this->entityManager->flush();
        return new CrudHttpResponse($this->serialize($relatedEntities));
    }

    public function addOneToMany($entityId, $relatedRepository, CrudHttpRequest $request)
    {
        $addMethodName = $this->annotationParser->getSetter($this->repository, $relatedRepository);
        $content = $request->getContent();
        $relatedEntity = $this->deserialize($content, $relatedRepository);
        $entity = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new CrudHttpResponse('', 404);
        }
        $validationErrors = $validator->validate($relatedEntity);
        if (count($validationErrors) > 0) {
            return new CrudHttpResponse((string) $validationErrors, 500);
        }
        $entity->$addMethodName($relatedEntity);
        $this->entityManager->flush();
        return new CrudHttpResponse($this->serialize($relatedEntity));
    }

    public function listRelated($entityId, $relatedRepository)
    {
        $listRelatedMethodName = $this->annotationParser->getListMethod($this->repository, $relatedRepository);
        $entity = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new CrudHttpResponse('', 404);
        }
        $related = $entity->$listRelatedMethodName();
        return new CrudHttpResponse($this->serialize($related));
    }

    public function removeRelated($entityId, $childId, $relatedRepository)
    {
        $removeRelatedMethod = $this->annotationParser->getRemoveMethod($this->repository, $relatedRepository);
        $parent = $this->entityManager
                ->getRepository($this->repository)
                ->find($entityId);
        $toRemove = $this->entityManager
                ->getRepository($relatedRepository)
                ->find($childId);
        if (!$parent || !$toRemove) {
            return new CrudHttpResponse('', 404);
        }
        $parent->$removeRelatedMethod($toRemove);
        $this->entityManager->flush();
        return new CrudHttpResponse('', 200);
    }

    public function setAnnotationParser(Interfaces\AnnotationParserInterface $parser)
    {
        $this->annotationParser = $parser;
        return $this;
    }

    public function setEntityManager(EntityManagerInterface $manager)
    {
        $this->entityManager = $manager;
        return $this;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
        return $this;
    }

    public function setSerializer(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
        return $this;
    }

    public function setValidator(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        return $this;
    }

    public function setFormat(Enum\RequestFormat $format)
    {
        $this->format = $format;
        return $this;
    }

    /**
     * deserialize requset json, validate entity and persist to DB
     * @param type $json
     * @return Response
     */
    private function upsertRequest($data, $repository)
    {
        $entity = $this->deserialize($data, $repository);
        $validationErrors = $this->validate($entity);
        if (count($validationErrors) > 0) {
            return new CrudHttpResponse((string) $validationErrors, 500);
        }
        $this->entityManager->persist($entity);
        $this->entityManager->flush();
        return new CrudHttpResponse($this->serialize($entity));
    }

    /**
     * wrapper for SerializerInterface::serialize with 'read' serialization context
     * @param Doctrine $entity
     * @return string serialized to CrudService::format
     */
    private function serialize($entity)
    {
        return $this->serializer->serialize($entity, $this->format, SerializationContext::create()->setGroups(array('read')));
    }

    /**
     * deserialize data to attributes of Entity marked as 'update' group
     * @param type $data raw data in CrudService::format
     * @param type $repository target entity repository, to deserialize into
     * @return Entity deserialized
     */
    private function deserialize($data, $repository)
    {
        return $this->serializer->deserialize($data, $repository, $this->format, DeserializationContext::create()->setGroups(array('update')));
    }

    /**
     * since validator is optional, run validation and return results only it was set,
     * otherwise just return empty validation errors array
     * @param Entity $entity
     * @return array
     */
    private function validate($entity)
    {
        return $this->validator ? $this->validator->validate($entity) : array();
    }

}
