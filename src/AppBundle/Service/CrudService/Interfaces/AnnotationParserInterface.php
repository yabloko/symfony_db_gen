<?php

namespace AppBundle\Service\CrudService\Interfaces;

/**
 *
 * @author andrew
 */
interface AnnotationParserInterface
{
    /**
     * get method name of $targetRepository entity to add ManyToMany $relatedRepository entities
     * 
     * @param string $targetRepository parent entity repository
     * @param string $relatedRepository related entity repository, a collection of which will be added to parent
     * @return string method name must comply with signature - addMethod(\Doctrine\Common\Collections\ArrayCollection $collection)
     * @throws AppBundle\Service\CrudService\Exception\EntityException if method does not exist
     */
    public function getAddMethod($targetRepository, $relatedRepository);
    
    /**
     * get method name of $targetRepository entity to add single OneToMany $relatedRepository entity
     * please note - relatedRepository entity here is what you actually create as Many part of the
     * relation via addOneToMany, but since Many entities can be added fron owing side only, 
     * targetRepository is an owing side here
     * 
     * @param string $targetRepository parent entity repository
     * @param string $relatedRepository repository of an entity to be added to parent as a child
     * @return string method name must comply with signature - setEntity({$relatedRepository} $entity) 
     * @throws AppBundle\Service\CrudService\Exception\EntityException if method does not exist
     */
    public function getSetter($targetRepository, $relatedRepository); 
    
    /**
     * get method to list all related entities
     * 
     * @param string $targetRepository parent entity repository
     * @param string $relatedRepository repository of an entity that will be listed
     * @return string method name must comply with signature \Doctrine\Common\Collections\ArrayCollection<$relatedRepository> listMethod()
     * @throws AppBundle\Service\CrudService\Exception\EntityException if method does not exist
     */
    public function getListMethod($targetRepository, $relatedRepository);
    
    /**
     * get method to remove entities of child type from parent entity
     * 
    * @param string $targetRepository parent entity repository
     * @param string $relatedRepository repository of an entity that will be listed
     * @return string method name must comply with signature removeMethod({$relatedRepository} $entity)
     * @throws AppBundle\Service\CrudService\Exception\EntityException if method does not exist
     */
    public function getRemoveMethod($targetRepository, $relatedRepository);
}
