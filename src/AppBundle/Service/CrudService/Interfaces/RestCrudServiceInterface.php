<?php

namespace AppBundle\Service\CrudService\Interfaces;

use AppBundle\Service\CrudService\Http\CrudHttpRequest;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use AppBundle\Service\CrudService\Enum\RequestFormat;

/**
 *
 * @author andrew
 */
interface RestCrudServiceInterface
{

    /**
     * create new entity from request
     * 
     * @param Request $request
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse entity encoded with encoder set
     */
    public function create(CrudHttpRequest $request);

    /**
     * list all repository entities
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse with serialized list of entities
     */
    public function listAll();

    /**
     * get single entity 
     * 
     * @param mixed $entityId
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse serialized entity | HTTP 404 if entity does not exist
     */
    public function read($entityId);

    /**
     * update existing entity, specified by id, with data from request
     * 
     * @param mixed $entityId
     * @param Request $request
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse serialized and updated entity | HTTP 404 if entity does not exist
     */
    public function update($entityId, CrudHttpRequest $request);

    /**
     * delete existing entity by id
     * 
     * @param mixed $entityId
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse HTTP 200 | 404 if entity does not exist
     */
    public function delete($entityId);

    /**
     * add many to many relation between existing entities. $request shoulв contain at least id field
     * of an entity to be aded as related to $entityId
     * $request can contain a sigle entity or a collection of entities for bulk add operation
     * 
     * @param mixed $entityId 
     * @param string $relatedRepository related entity repository path
     * @param Request $request 
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse HTTP 201 | HTTP 404 if $entityId not found 
     * all related entities (bulk add) do not exist. in case some
     * related entities from bulk add do no exist, they will be silently omitted
     * @throws AppBundle\Service\CrudService\Exception\EntityException
     */
    public function addManyToMany($entityId, $relatedRepository, CrudHttpRequest $request);

    /**
     * add one to many relation between entities. this kind of a relation can be aded from owing side only
     * 
     * @param mixed $entityId
     * @param string $relatedRepository
     * @param Request $request containing new related entity
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse HTTP 201 | HTTP 404 if $entityId or related entity not found
     * @throws AppBundle\Service\CrudService\Exception\EntityException
     */
    public function addOneToMany($entityId, $relatedRepository, CrudHttpRequest $request);

    /**
     * list related entities by parent entityId and related type, 
     * list getter is resolved by annotation
     * 
     * @see AnnotationParserInterface
     * @param mixed $entityId
     * @param string $relatedRepository
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse HTTP 201 | HTTP 404 if $entityId not found
     * @throws AppBundle\Service\CrudService\Exception\EntityException
     */
    public function listRelated($entityId, $relatedRepository);

    /**
     * remove related entity from $entityId, relatd entity is resolved by its id
     * and annotaton
     * 
     * @see AnnotationParserInterface
     * @param type $entityId
     * @param type $childId
     * @param type $relatedRepository
     * @return AppBundle\Service\CrudService\Http\CrudHttpResponse HTTP 200 | HTTP 404 if $entityId or $childId not found
     * @throws AppBundle\Service\CrudService\Exception\EntityException
     */
    public function removeRelated($entityId, $childId, $relatedRepository);

    /**
     * set entity repository name
     * 
     * @param string $repository
     * @return AppBundle\Service\CrudService\CrudService self
     */
    public function setRepository($repository);

    /**
     * 
     * @param mixed $serializer
     * @return AppBundle\Service\CrudService\CrudService self
     */
    public function setSerializer(SerializerInterface $serializer);

    /**
     * 
     * @param \Doctrine\ORM\EntityManagerInterface $manager
     * @return AppBundle\Service\CrudService\CrudService self
     */
    public function setEntityManager(EntityManagerInterface $manager);

    /**
     * 
     * @param \AppBundle\Service\CrudService\Interfaces\AnnotationParserInterface $parser
     * @return AppBundle\Service\CrudService\CrudService self
     */
    public function setAnnotationParser(AnnotationParserInterface $parser);

    /**
     * 
     * @param Symfony\Component\Validator\Validator\ValidatorInterface $validator
     * @return AppBundle\Service\CrudService\CrudService self
     */
    public function setValidator(ValidatorInterface $validator);

    /**
     * 
     * @param RequestFormat $format
     * @return AppBundle\Service\CrudService\CrudService self
     */
    public function setFormat(RequestFormat $format);
}
