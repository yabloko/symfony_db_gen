<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/tag")
 */
class TagController extends RestCrudController
{

    protected $repository = 'AppBundle\Entity\Tag';

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function listAction()
    {
        return parent::listAction();
    }

    /**
     * @Route("/{tagId}")
     * @Method({"GET"})
     */
    public function readAction($tagId)
    {
        return parent::readAction($tagId);
    }

    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * @Route("/{tagId}")
     * @Method({"PUT"})
     */
    public function updateAction($tagId, Request $request)
    {
        return parent::updateAction($tagId, $request);
    }

    /**
     * @Route("/{tagId}")
     * @Method({"DELETE"})
     */
    public function deleteAction($tagId)
    {
        return parent::deleteAction($tagId);
    }

    /**
     * @ROUTE("/{tagId}/post")
     * @Method({"GET"})
     */
    public function listPostsAction($tagId)
    {
        return $this->listRelated($tagId, 'getPosts');
    }

    /**
     * @ROUTE("/{tagId}/user")
     * @Method({"GET"})
     */
    public function listUsersAction($tagId)
    {
        return $this->listRelated($tagId, 'getUsers');
    }
}
