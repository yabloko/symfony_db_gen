<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;

/**
 * Description of RestCrudController
 *
 * @author andrew
 */
abstract class RestCrudController extends Controller
{

    use \AppBundle\Controller\Traits\JsonTrait;

    protected $repository;
    protected $format = 'json';

    /**
     * create AppBundle:Entity from request data
     * map data from request to Entity with serializer
     * @param Request $request
     * @return Response
     */
    public function createAction(Request $request)
    {
        return $this->upsertRequest($request->getContent(), $this->repository);
    }

    /**
     * list all AppBundle:Entity items
     * @return Response
     */
    public function listAction()
    {
        $users = $this->getDoctrine()
                ->getRepository($this->repository)
                ->findAll();
        $serializer = $this->get('serializer');
        $response = $serializer->serialize($users, $this->format, SerializationContext::create()->setGroups(array('read')));
        return new Response($response);
    }

    /**
     * get data from AppBundle:Entity by given id, render only fields of "read" group,
     * return NOT FOUND if no entity with given id exists
     * @param integer $entityId
     * @return Response data or HTTP 404
     */
    public function readAction($entityId)
    {
        $user = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$user) {
            return new Response('', 404);
        }
        $serializer = $this->get('serializer');
        $response = $serializer->serialize($user, $this->format, SerializationContext::create()->setGroups(array('read')));
        return new Response($response);
    }

    /**
     * update AppBundle:Entity by id with sent data, map only "update" group
     * fields with serializer
     * @param integer $entityId
     * @param Request $request
     * @return Response
     */
    public function updateAction($entityId, Request $request)
    {
        $entity = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new Response('', 404);
        }
        $content = $request->getContent();
        $contentWithId = $this->addIdToJson($content, $entity->getId());
        return $this->upsertRequest($contentWithId, $this->repository);
    }

    /**
     * delete AppBundle:Entity by given id
     * @param integer $entityId
     * @return Response HTTP 200 on delete | HTTP 404 if entity does not exist
     */
    public function deleteAction($entityId)
    {
        $entity = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new Response('', 404);
        }
        $entityManager = $this->getDoctrine()
                ->getManager();
        $entityManager->remove($entity);
        $entityManager->flush();
        return new Response('', 200);
    }

    /**
     * add record for ManyToMany relation
     * @param integer $entityId
     * @param string $relatedEntityRepository
     * @param Request $request
     * @param string $addMethodName - should be figured by annotation
     * @param bool $isCollection - should be figured by annotation
     * @return Response
     */
    protected function addManyToManyAction($entityId, $relatedEntityRepository, Request $request, $addMethodName, $isCollection = false)
    {
        $serializer = $this->get('serializer');
        $content = $request->getContent();
        $deserializerRepository = $isCollection ? 'ArrayCollection<' . $relatedEntityRepository . '>' : $relatedEntityRepository;
        $requestedEntities = $serializer->deserialize($content, $deserializerRepository, $this->format, DeserializationContext::create()->setGroups(array('read')));
        if (!is_array($requestedEntities)) {
            $requestedEntities = array($requestedEntities);
        }
        $ids = array_map(function($related) {
            return $related->getId();
        }, $requestedEntities);
        $entity = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($entityId);
        $relatedEntities = $this->getDoctrine()
                ->getRepository($relatedEntityRepository)
                ->findBy(array('id' => $ids));
        if (!$entity || count($relatedEntities) < 1) {
            return new Response('', 404);
        }
        $entity->$addMethodName($relatedEntities);
        $entityManager = $this->getDoctrine()
                ->getManager();
        $entityManager->flush();
        $response = $serializer->serialize($relatedEntities, $this->format, SerializationContext::create()->setGroups(array('read')));
        return new Response($response);
    }

    /**
     * create OneToMany related entity for given record
     * @param integer $entityId One entity Id
     * @param string $relatedEntityRepository - related entity repository
     * @param Request $request
     * @param string $addMethodName method to add related entities
     * @param bool $isCollection
     * @return Response
     */
    protected function addOneToManyAction($entityId, $relatedEntityRepository, Request $request, $addMethodName)
    {
        $serializer = $this->get('serializer');
        $content = $request->getContent();
        $relatedEntity = $serializer->deserialize($content, $relatedEntityRepository, $this->format, DeserializationContext::create()->setGroups(array('read')));

        $entity = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new Response('', 404);
        }
        $validator = $this->get('validator');
        $validationErrors = $validator->validate($relatedEntity);
        if (count($validationErrors) > 0) {
            return new Response((string) $validationErrors, 500);
        }
        $entity->$addMethodName($relatedEntity);
        $entityManager = $this->getDoctrine()
                ->getManager();
        $entityManager->flush();
        $response = $serializer->serialize($relatedEntity, $this->format, SerializationContext::create()->setGroups(array('read')));
        return new Response($response);
    }

    /**
     * remove related entity or relationship
     * @param int $parentId
     * @param int $relatedId
     * @param string $relatedRepository
     * @param string $removeMethodName
     * @return Response
     */
    protected function removeRelated($parentId, $relatedId, $relatedRepository, $removeMethodName)
    {
        $parent = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($parentId);
        $toRemove = $this->getDoctrine()
                ->getRepository($relatedRepository)
                ->find($relatedId);
        if (!$parent || !$toRemove) {
            return new Response('', 404);
        }
        $parent->$removeMethodName($toRemove);
        $this->getDoctrine()
                ->getManager()
                ->flush();
        return new Response('', 200);
    }

    /**
     * list related entities
     * @param integer $entityId
     * @param string $listGetterName
     * @return Response
     */
    protected function listRelated($entityId, $listGetterName)
    {
        $entity = $this->getDoctrine()
                ->getRepository($this->repository)
                ->find($entityId);
        if (!$entity) {
            return new Response('', 404);
        }
        $related = $entity->$listGetterName();
        $serializer = $this->get('serializer');
        $response = $serializer->serialize($related, $this->format, SerializationContext::create()->setGroups(array('read')));
        return new Response($response);
    }

    /**
     * deserialize requset json, validate entity and persist to DB
     * @param type $json
     * @return Response
     */
    private function upsertRequest($json, $repository)
    {
        $serializer = $this->get('serializer');
        $entity = $serializer->deserialize($json, $repository, $this->format, DeserializationContext::create()->setGroups(array('update')));
        $validator = $this->get('validator');
        $validationErrors = $validator->validate($entity);
        if (count($validationErrors) > 0) {
            return new Response((string) $validationErrors, 500);
        }
        $entityManager = $this->getDoctrine()
                ->getManager();
        $entityManager->persist($entity);
        $entityManager->flush();
        $response = $serializer->serialize($entity, $this->format, SerializationContext::create()->setGroups(array('read')));
        return new Response($response);
    }

}
