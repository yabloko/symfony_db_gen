<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * PostController
 * 
 * @Route("/post")
 * 
 * @author andrew
 */
class PostController extends RestCrudController
{

    use Traits\TaggableTrait;

    protected $repository = 'AppBundle\Entity\Post';

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function listAction()
    {
        return parent::listAction();
    }

    /**
     * @Route("/{postId}")
     * @Method({"GET"})
     */
    public function readAction($postId)
    {
        return parent::readAction($postId);
    }

    /**
     * @Route("/{postId}")
     * @Method({"PUT"})
     */
    public function updateAction($postId, Request $request)
    {
        return parent::updateAction($postId, $request);
    }

    /**
     * @Route("/{postId}")
     * @Method({"DELETE"})
     */
    public function deleteAction($postId)
    {
        return parent::deleteAction($postId);
    }

    /**
     * @Route("/{postId}/tag")
     * @Method({"GET"})
     */
    public function listTagsAction($postId)
    {
        return $this->listTags($postId);
    }

    /**
     * @Route("/{postId}/tag")
     * @Method({"PUT"})
     * @todo add tag list validation, 404 if some tag is missing?
     */
    public function addTagsAction($postId, Request $request)
    {
        return $this->addTags($postId, $request);
    }

    /**
     * @Route("/{postId}/tag/{tagId}")
     * @Method({"DELETE"})
     */
    public function removeTagsAction($postId, $tagId)
    {
        return $this->removeTag($postId, $tagId);
    }

}
