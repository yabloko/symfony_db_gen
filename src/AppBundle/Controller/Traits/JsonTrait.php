<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Controller\Traits;

/**
 * Description of JsonHelperTrait
 *
 * @author andrew
 */
trait JsonTrait
{

    /**
     * add "id" node to json root
     * @param string $json
     * @param mixed $id
     * @return string
     */
    function addIdToJson($json, $id)
    {
        $decodedJson = json_decode($json, true);
        $decodedJson['id'] = $id;
        return json_encode($decodedJson);
    }

}
