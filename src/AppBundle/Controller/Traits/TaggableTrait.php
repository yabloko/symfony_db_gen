<?php

namespace AppBundle\Controller\Traits;

use JMS\Serializer\SerializationContext;
use JMS\Serializer\DeserializationContext;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * TaggableTrait provides reusable code for controllers that
 * give access to taggable entities
 *
 * @author andrew
 */
trait TaggableTrait
{

    protected function listTags($entityId)
    {
        if(!is_a($this->repository, 'AppBundle\Entity\Interfaces\TaggableEntity', true)){
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, 'repository entity is not an implementation of AppBundle\Entity\Interfaces\TaggableEntity');
        }
        return $this->listRelated($entityId, 'getTags');
    }

    protected function addTags($userId, Request $request)
    {
        if(!is_a($this->repository, 'AppBundle\Entity\Interfaces\TaggableEntity', true)){
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, 'repository entity is not an implementation of AppBundle\Entity\Interfaces\TaggableEntity');
        }
        return $this->addManyToManyAction($userId, 'AppBundle\Entity\Tag', $request, 'addTags', true);
    }

    protected function removeTag($entityId, $tagId)
    {
        if(!is_a($this->repository, 'AppBundle\Entity\Interfaces\TaggableEntity', true)){
            throw new \Symfony\Component\HttpKernel\Exception\HttpException(500, 'repository entity is not an implementation of AppBundle\Entity\Interfaces\TaggableEntity');
        }
        return $this->removeRelated($entityId, $tagId, 'AppBundle\Entity\Tag');
    }

}
