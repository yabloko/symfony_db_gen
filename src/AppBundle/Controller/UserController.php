<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/user")
 */
class UserController extends RestCrudController
{

    use Traits\TaggableTrait;

    protected $repository = 'AppBundle\Entity\ApplicationUser';

    /**
     * @Route("/")
     * @Method({"POST"})
     */
    public function createAction(Request $request)
    {
        return parent::createAction($request);
    }

    /**
     * @Route("/")
     * @Method({"GET"})
     */
    public function listAction()
    {
        return parent::listAction();
    }

    /**
     * @Route("/{userId}")
     * @Method({"GET"})
     */
    public function readAction($userId)
    {
        return parent::readAction($userId);
    }

    /**
     * @Route("/{userId}")
     * @Method({"PUT"})
     */
    public function updateAction($userId, Request $request)
    {
        return parent::updateAction($userId, $request);
    }

    /**
     * @Route("/{userId}")
     * @Method({"DELETE"})
     */
    public function deleteAction($userId)
    {
        return parent::deleteAction($userId);
    }

    /**
     * @Route("/{userId}/tag")
     * @Method({"GET"})
     * @todo move to trait and use for all taggable entities controller
     */
    public function listTagsAction($userId)
    {
        return $this->listTags($userId);
    }

    /**
     * @Route("/{userId}/tag")
     * @Method({"PUT"})
     * @todo add tag list validation, 404 if some tag is missing?
     */
    public function addTagsAction($userId, Request $request)
    {
        return $this->addTags($userId, $request);
    }

    /**
     * @Route("/{userId}/tag/{tagId}")
     * @Method({"DELETE"})
     */
    public function removeTagsAction($userId, $tagId)
    {
        return $this->removeTag($userId, $tagId, 'removeTag');
    }

    /**
     * @Route("/{userId}/post")
     * @Method({"GET"})
     */
    public function listPostsAction($userId)
    {
        return $this->listRelated($userId, 'getPosts');
    }

    /**
     * @Route("/{userId}/post")
     * @Method({"POST"})
     * @todo maybe automated relation loading?
     */
    public function addPostAction($userId, Request $request)
    {
        return $this->addOneToManyAction($userId, 'AppBundle\Entity\Post', $request, 'addPost');
    }

}
