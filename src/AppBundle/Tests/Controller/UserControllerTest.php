<?php

namespace AppBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class UserControllerTest extends WebTestCase
{
    public function testCreate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user');
    }

    public function testRead()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/{userId}');
    }

    public function testUpdate()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/{userId}');
    }

    public function testDelete()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/{userId}');
    }

    public function testListtags()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/{userId}/tags');
    }

    public function testListposts()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/user/{userId}/posts');
    }

}
