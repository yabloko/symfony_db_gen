symfony_db_gen
==============

A Symfony project created on January 13, 2016, 5:00 pm.

a simple example of db generated based on bucket of models

an ApplicationUser
a Post 
and Tag

ApplicationUser can write a post, add a tag to a post or other user
everybody can see a tag on every post or user
post can be tagged with a certain tag only once, regardless of a user who tagged a post
same applies to user - user can have only unique tags, no duplicates

add a friendship between users - a user can be friend with any other user, excluding himself.
friendship list is unique.

add reporting or mentorship relation - a user can mentor other user(s?) and can have one single mentor
